def say_goodnight(name)
  result = "Good night, " + name
  return result
end

# Time for bed ...

puts say_goodnight ("Johnny boy")
puts say_goodnight ("Mary-ellen")

#  # expression interpolaion would work as well
# def say_goodnight(name)
#   result = "Good night, #{name}"
#   return result
# end

# # Time for bed ...

# puts say_goodnight ("Johnny boy")
# puts say_goodnight ("Mary-ellen")
