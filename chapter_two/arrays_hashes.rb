a = [ 1, 'cat', 3.14 ]  # array with three elements
puts "The first element is #{a[0]}"
# set the third element
a[2] = nil
puts "the array is now #{a.inspect}"

# arrays of words

a = [ 'ant', 'bee', 'cat', 'dog', 'elk']
a[0]
a[3]
# this is the same:
a = %w{ ant bee cat dog elk}

a[0]          # => "ant"
a[3]          # => "dog"

p a

# Hashes

inst_section = {
  'cello' =>  'string',
  'clarinet' => 'woodwind',
  'drum'  =>  ' percussion',
  'oboe' => 'woodwind',
  'trumpet' => 'brass',
  'violin'  => 'string'
}
p inst_section['oboe']
p inst_section['cello']
p inst_section['bassoon']

histogram = Hash.new(0) # The default is zero
histogram['ruby'] # => 0
histogram['ruby'] = histogram['ruby'] + 1
histogram['ruby'] # => 1
