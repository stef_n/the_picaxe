today = Time.now

if today.saturday?
  puts "Do chores around the house"
elsif today.sunday?
  puts "Relax"
else
  puts "Go to work"
end

# condensing with statement modifiers

if radiation > 3000
  puts "Danger, Will Robinson"
end

# Becomes
puts "Danger, Will Robinson" if radiation > 3000

# Similarly, with the While loop

square = 4
while square < 1000
  square = square*square
end

# becomes more concise:
square = 4
square = square*square while square < 1000
