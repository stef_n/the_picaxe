# You can think of yield as being something like a method call that invokes
# the block associated with the call to the method containing the yield.

def call_block
  puts "Start of method"
  yield
  yield
  yield
  puts "End of method"
end

call_block { puts "In the block"}
puts # To have a line break between examples


# You can also provide arguments to the call to yield, and they will be
# passed to the block. Within the block, you list the names of the parameters
# to receive these arguments between vertical bars (|params...|). This example
# shows a method calling its associated block twice, passing the block
# two arguments each time:

def who_says_what
  yield("Dave", "hello")
  yield("Andy", "goodbye")
end

who_says_what { |person, phrase| puts "#{person} says #{phrase}"}
puts # To have a line break between examples

# Here is an example of implementing iterators

animals = %w( ant bee cat dog ) # create an array
animals.each { |animal| puts animal} # iterate over the contents

puts # To have a line break between examples

[ 'cat', 'dog', 'horse' ].each { |name| print name, " " }
5.times { print "*" }
3.upto(6).each { |char| print char}
puts
