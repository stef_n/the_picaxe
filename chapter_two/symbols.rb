def walk(direction)
  if direction == :north
    # . . .
  end
end


# The hash syntax can be shortened with symbols

inst_section = {
  cello:  'string',
  clarinet: 'woodwind',
  drum:   ' percussion',
  oboe:   'woodwind',
  trumpet: 'brass',
  violin:  'string'
}

puts "An oboe is a #{inst_section[:oboe]} instrument"
